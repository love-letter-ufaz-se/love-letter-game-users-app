FROM golang:1.14.1
WORKDIR /go/src/users_app/
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...

RUN chmod +x ./start.sh
RUN apt-get update
RUN apt -y install netcat \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent

RUN curl -sSL https://packagecloud.io/golang-migrate/migrate/gpgkey | apt-key add -
RUN echo "deb https://packagecloud.io/golang-migrate/migrate/ubuntu/ bionic main" > /etc/apt/sources.list.d/migrate.list
RUN apt-get update && \
    apt-get install -y migrate

EXPOSE 8080

CMD bash start.sh