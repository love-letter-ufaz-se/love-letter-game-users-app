module gitlab.com/love-letter-ufaz-se/love-letter-game-users-app/v2

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.3.5
	github.com/gorilla/mux v1.7.4
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
	google.golang.org/grpc v1.28.1
)
