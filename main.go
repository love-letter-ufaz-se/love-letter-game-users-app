package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-users-app/v2/session"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
)

var db *sql.DB

type createUserSTR struct {
	Email    string `json:"email"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type signInUserSTR struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func main() {

	var err error
	//MySQL Client initiation
	{
		db, err = sql.Open("mysql", "root:root@tcp(mysql-db:3306)/users")

		handleErr("Could not create connection with DB", err)
		defer db.Close()

		err = db.Ping()

		handleErr("Could not ping DB", err)
	}

	// Router Initiation
	{
		r := mux.NewRouter()
		r.Use(mux.CORSMethodMiddleware(r))
		s := r.PathPrefix("/users/public/").Subrouter()
		s.HandleFunc("/user", createUser).Methods("POST")
		s.HandleFunc("/signIn", signIn).Methods("POST")
		s.HandleFunc("/unique-ID", getUniqueID)
		s.HandleFunc("/guest-user", createGuestUser).Methods("POST")
		s.HandleFunc("/user-name", getUserName)

		private := r.PathPrefix("/users/private/").Subrouter()

		private.Use(auth)
		private.HandleFunc("/users/", showPrivate)

		log.Fatal(http.ListenAndServe(":8080", r))
	}

}

func createUser(w http.ResponseWriter, r *http.Request) {

	var newUser createUserSTR

	reqBody, err := ioutil.ReadAll(r.Body)
	handleErr("Can't read request body for createUser", err)

	err = json.Unmarshal(reqBody, &newUser)
	handleErr("Can't unmarshal reqBody to newUser for createUser", err)

	stmtEmail, err := db.Prepare("SELECT id FROM users WHERE email = ?")
	defer stmtEmail.Close()
	handleErr("There was a problem with prepared statement for signIn", err)

	var uid int
	err = stmtEmail.QueryRow(newUser.Email).Scan(&uid)

	if err != nil {
		log.Printf(err.Error())
		stmt, err := db.Prepare("INSERT INTO users(email, password, name) VALUES (?, ?, ?)")
		handleErr("Can't create prepared statement for createUser", err)
		defer stmt.Close()

		hashedPass, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), bcrypt.DefaultCost)
		handleErr("Can't bcrypt password for createUser", err)

		_, err = stmt.Exec(newUser.Email, string(hashedPass), newUser.Name)
		handleErr("Can't execute prepared statement for createUser", err)

		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(newUser)

	} else {
		handleErr(" This email has been registered before", err)
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(" This email has been registered before"))
	}

}

func showPrivate(w http.ResponseWriter, r *http.Request) {
	log.Println("Test this private")
	w.Write([]byte(" test this private\n"))
}

func signIn(w http.ResponseWriter, r *http.Request) {
	var userEmail signInUserSTR
	reqBody, err := ioutil.ReadAll(r.Body)
	handleErr("Can't read request body for signIn", err)
	err = json.Unmarshal(reqBody, &userEmail)

	stmt, err := db.Prepare("SELECT id, password FROM users WHERE email = ?")
	defer stmt.Close()
	handleErr("There was a problem with prepared statement for signIn", err)
	var uid int
	var upass string
	err = stmt.QueryRow(userEmail.Email).Scan(&uid, &upass)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Not Authorized"))
	} else {
		if err = bcrypt.CompareHashAndPassword([]byte(upass), []byte(userEmail.Password)); err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Not Authorized"))
		} else {
			opts := grpc.WithInsecure()
			sessionGRPC, err := grpc.Dial("session-app:50051", opts)
			handleErr("Could not create connection between session-app", err)
			defer sessionGRPC.Close()

			client := session.NewSessionServiceClient(sessionGRPC)
			request := &session.UserID{
				ID: int64(uid),
			}
			resp, err := client.NewUserSession(context.Background(), request)
			handleErr("Error returned from session app for signIn", err)
			setSession(resp.NewCookie, w)
			w.WriteHeader(http.StatusAccepted)
			json.NewEncoder(w).Encode(userEmail.Email)
		}
	}
}

func auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		cookie, err := r.Cookie("lovelettersession")
		if err != nil {
			log.Println("Auth not letting in!")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Not Allowed\n"))
			return
		}

		cookieVal := cookie.Value

		opts := grpc.WithInsecure()
		sessionGRPC, err := grpc.Dial("session-app:50051", opts)
		handleErr("Could not create connection between session-app", err)
		defer sessionGRPC.Close()

		client := session.NewSessionServiceClient(sessionGRPC)
		request := &session.SessionCookie{
			Cookie: cookieVal,
		}
		resp, err := client.CheckSession(context.Background(), request)
		handleErr("Error returned from session app for auth", err)
		log.Printf("Auth received response: %v", resp.Session)
		if resp.Session != "guest" {
			next.ServeHTTP(w, r)
		} else {
			log.Println("Auth not letting in!")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Not Allowed\n"))
			return
		}
	})
}

func createGuestUser(w http.ResponseWriter, r *http.Request) {

	opts := grpc.WithInsecure()
	sessionGRPC, err := grpc.Dial("session-app:50051", opts)
	handleErr("Could not create connection between session-app", err)
	defer sessionGRPC.Close()
	client := session.NewSessionServiceClient(sessionGRPC)
	request := &session.NoParams{}

	resp, err := client.NewGuestSession(context.Background(), request)
	handleErr("Error returned from session app for guest user", err)
	setSession(resp.NewCookie, w)
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte("Session created successfully\n"))
}

func getUniqueID(w http.ResponseWriter, r *http.Request) {

	cookie, err := r.Cookie("lovelettersession")
	if err != nil {
		log.Println("Auth not letting in!")
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Not Allowed\n"))
		return
	}

	cookieVal := cookie.Value
	opts := grpc.WithInsecure()
	sessionGRPC, err := grpc.Dial("session-app:50051", opts)
	handleErr("Could not create connection between session-app", err)
	defer sessionGRPC.Close()
	client := session.NewSessionServiceClient(sessionGRPC)
	request := &session.SessionCookie{
		Cookie: cookieVal,
	}

	resp, err := client.GetUniqueID(context.Background(), request)
	handleErr("Error returned from session app for auth", err)
	log.Printf("Auth received response: %v", resp.Session)

	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(resp.Session)

}

func getUserName(w http.ResponseWriter, r *http.Request) {

	// get cookie
	cookie, err := r.Cookie("lovelettersession")
	if err != nil {
		log.Println("Auth not letting in!")
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Not Allowed\n"))
		return
	}

	cookieVal := cookie.Value

	// create session
	opts := grpc.WithInsecure()
	sessionGRPC, err := grpc.Dial("session-app:50051", opts)
	handleErr("Could not create connection between session-app", err)
	defer sessionGRPC.Close()

	client := session.NewSessionServiceClient(sessionGRPC)
	request := &session.SessionCookie{
		Cookie: cookieVal,
	}

	resp, err := client.CheckSession(context.Background(), request)
	handleErr("Error returned from session app for auth", err)
	log.Printf("Auth received response: %v", resp.Session)

	if resp.Session != "guest" {
		var userName string

		stmt, err := db.Prepare("SELECT name FROM users WHERE id = ?")
		defer stmt.Close()
		handleErr("There was a problem with prepared statement for signIn", err)

		err = stmt.QueryRow(resp.Session).Scan(&userName)

		if err != nil {
			//	handleErr("No user found in DB", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Not Authorized"))
		} else {
			w.WriteHeader(http.StatusAccepted)
			json.NewEncoder(w).Encode(userName)
		}
	} else {
		w.WriteHeader(http.StatusAccepted)
		json.NewEncoder(w).Encode("Guest")
	}

}

func handleErr(msg string, err error) {
	if err != nil {
		log.Fatalf("%s ... %v\n", msg, err)
	}
}

func setSession(token string, response http.ResponseWriter) {

	cookie := &http.Cookie{
		Name:   "lovelettersession",
		Value:  token,
		Path:   "/",
		MaxAge: 86400,
	}
	http.SetCookie(response, cookie)

}
